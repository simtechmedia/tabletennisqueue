(function ($) {
    'use strict';
    var socket ,
        DATA_MODEL = {
            init : function() {
            },

            addPlayers : function(players) {
                VIEW.addPlayers(players);
            },

            removePlayers : function ( uid ) {
                console.log('removePLayers ' + uid);
                VIEW.removePlayers(uid);
            },

            sendAddPlayer : function ( firstPlayer , secondPlayer ) {

                console.log('sendAddPlayer = ' + firstPlayer + ' secondPlayer = ' + secondPlayer );

                var uid = DATA_MODEL.guid();
                var playerOb = {
                    'uid'           : uid ,
                    'firstPlayer'  : firstPlayer
                };

                if( secondPlayer === "") {
                    socket.emit('waitingPlayer', playerOb );
                } else {
                    playerOb.secondPlayer = secondPlayer;

                    socket.emit('addPlayers', playerOb );
                }

            },

            sendRemovePlayer : function ( uid ) {
                socket.emit('removePlayers', uid );
            } ,

            sendGameWinner : function ( uid , winner ) {
                console.log("sending gameFinished " + uid + " " + winner );
                socket.emit('gameFinished' , uid , winner );
            },
            // Generator unique ID
            guid :(function() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return function() {
                    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                        s4() + '-' + s4() + s4() + s4();
                };
            })()

        },
        SOCKET_LISTENERS = {
            init : function(){

                socket =  io('http://localhost:3000');

                // Renders Player List on connect
                socket.on('queuedGames', VIEW.renderPlayerList);

                // Adds Playera on Connect
                socket.on('addPlayers', DATA_MODEL.addPlayers)

                // Remove Players
                socket.on('removePlayers', DATA_MODEL.removePlayers);
            }
        },
        VIEW = {
            renderPlayerList : function( playerList  ) {
                $.each( playerList , function (i, item) {
                    var templateSource  = $('#' + 'players' + '-template').html(),
                        template        = Handlebars.compile(templateSource),
                        html            = template(item);
                    $('#player-list .player-list').append(html);
                    $(html).hide().fadeIn();
                });
            } ,

            addPlayers : function ( players ) {
                var templateSource  = $('#' + 'players' + '-template').html(),
                    template        = Handlebars.compile(templateSource),
                    html            = template(players);
                $('#player-list .player-list').append(html);
                $(html).hide().fadeIn();
            } ,

            removePlayers : function ( uid ) {
                $('.player-list li'+"[data-uid='"+uid+"']").fadeOut(300, function(){ $(this).remove(); });
            }
        } ,
        // View Listeners
        VIEW_UI = {
            init : function(){

                // Add button click
                $('#addPlayerButton').click(function(){
                    console.log("did i do dat ");
                    $('.typeahead').typeahead('close');
                    DATA_MODEL.sendAddPlayer( $('#firstplayer').val() , $('#secondplayer').val() );
                    $('#firstplayer').val('');
                    $('#secondplayer').val('');

                    return false;
                });

                // On remove button click
                $('.player-list').on('click', 'button' , function( ){
                    var uid =  $(this).parent().data('uid') ;
                    console.log( $(this) );
                    console.log( $(this)[0].innerText ) ;
                    switch( $(this).data('attributes') ) {
                        case 'winner' :
                            DATA_MODEL.sendGameWinner(uid, $(this)[0].innerText   )
                            break;
                        case 'cancel' :
                            DATA_MODEL.sendRemovePlayer(uid);
                            break;
                    }
                });
                $( ".newplayers" ).submit(function( event ) {
                    alert( "Handler for .submit() called." );
                    event.preventDefault();
                });
                //VIEW_UI.addValidation();
                VIEW_UI.addAutoComplete();
            } ,
            addValidation : function() {
                //
                $('#addName').bootstrapValidator({
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        firstplayer: {
                            validators: {
                                notEmpty: {
                                    message: 'This field is required'
                                }
                            }
                        },
                        secondplayer: {
                            validators: {
                                notEmpty: {
                                    message: 'This field is required'
                                }
                            }
                        }
                    }
                });
            },

            addAutoComplete : function() {
                var names = new Bloodhound({
                    local: $.map( staffData, function(name) {
                        return {
                            firstName: name.FirstName ,
                            lastName : name.Surname
                        };
                    }),
                    datumTokenizer: function(d){
                        var firstName = Bloodhound.tokenizers.whitespace(d.firstName),
                            lastName = Bloodhound.tokenizers.whitespace(d.lastName);
                        return firstName.concat(lastName);
                    },
                    queryTokenizer:Bloodhound.tokenizers.whitespace,
                    limit: 10
                });
                names.initialize();

                $('.typeahead').typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 1
                    },
                    {
                        name: 'names',
                        displayKey: function(name) {
                            return name.firstName + ' ' + name.lastName;
                        } ,
                        source: names.ttAdapter(),
                        templates : {
                            suggestion : Handlebars.compile([
                                '<p class="">{{firstName}} <span>{{lastName}}</span></p>'
                            ].join(''))
                        }
                    })
                }
        }
    $.fn.ttbInit = function(){
        console.log('table tennis init');
        // Start
        DATA_MODEL.init();
        SOCKET_LISTENERS.init();
        VIEW_UI.init();
    };
}(jQuery));

$(document).ready(function(){
    'use strict';
    $(this).ttbInit();
});
