
// File Server
var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic("/Applications/XAMPP/xamppfiles/htdocs/ResearchAndDevelopment/tabletennisbooking/server/public/dist/")).listen(8080);


// Socket.io
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var mongoose = require('mongoose');

// Save winner to Database
var gamesSchema = mongoose.Schema({
    timeStamp : Number ,
    players : [{
        player : String
    }] ,
    winner : String
});

// Queued List
var queuedGames = [
    {"firstPlayer":"Simon" , "secondPlayer": "Dan" , "uid" : "e773a925-9a8d-d996-36ed-c778ebc80665" }
]

var lookingForGames = [];

io.on('connection', function(socket){
    console.log('a user connected');
    socket.emit('queuedGames', queuedGames );

    var removeGame = function (uid) {
        io.emit('removePlayers', uid );
        queuedGames.splice( findIndex(uid) ,1);
    };

    var findIndex = function ( uid ) {
        var index = 0;
        for (var i = 0; i < queuedGames.length; i++) {
            if(queuedGames[i]["uid"] === uid ) {
                index = i;
                console.log("index " + i ) ;
//              queuedGames.splice(i,1);
            }
        }
        return index;
    };

    socket.on('addPlayers', function( playerOb ) {
        queuedGames.push(playerOb);
        io.emit('addPlayers', playerOb );
    });

    socket.on('waitingPlayer', function( playerOb ){
       console.log("waitingPlayer");

       console.log(playerOb);

    });

    socket.on('removePlayers', function( uid ){
        removeGame(uid);
    });

    socket.on('gameFinished', function( uid , winner ){
        console.log("recieved gameFinished " + uid  + " winner = " + winner );

        var index = findIndex(uid);

        var Game = mongoose.model('Game', gamesSchema);
        var gameVO = new Game({
            timeStamp : Date.now() ,
            players : [
                { player : queuedGames[index].firstPlayer },
                { player : queuedGames[index].secondPlayer }
            ],
            winner : winner
        });
        console.log( gameVO );

        gameVO.save(function (err, game1) {
            if (err) return console.error(err);
            console.log("Save worked");
        });
        removeGame(uid);
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});

// DB
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
    // yay!
    console.log("DB Open");

});

